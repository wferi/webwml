<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The daemon in GDM does not properly unexport display objects from its
D-Bus interface when they are destroyed, which allows a local attacker
to trigger a use-after-free via a specially crafted sequence of D-Bus
method calls, resulting in a denial of service or potential code
execution.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.14.1-7+deb8u1.</p>

<p>We recommend that you upgrade your gdm3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1494.data"
# $Id: $
