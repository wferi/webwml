<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Magnus K. Stubman found that an authenticated remote attacker could
crash an instance of Net-SNMP by sending a specially crafted UDP packet
resulting in a denial-of-service.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.7.2.1+dfsg-1+deb8u2.</p>

<p>We recommend that you upgrade your net-snmp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1540.data"
# $Id: $
