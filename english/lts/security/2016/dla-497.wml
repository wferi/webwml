<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the dissectors/parsers for
PKTC, IAX2, GSM CBCH and NCP which could result in denial of service.</p>

<p>This update also fixes many older less important issues by updating the
package to the version found in Debian 8 also known as Jessie.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u6~deb7u1.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-497.data"
# $Id: $
