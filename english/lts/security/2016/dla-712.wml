<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9445">CVE-2016-9445</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9446">CVE-2016-9446</a>

    <p>Chris Evans discovered that the GStreamer plugin to decode VMware screen
    capture files allowed the execution of arbitrary code. He also found that
    an initialized buffer may lead into memory disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9447">CVE-2016-9447</a>

    <p>Chris Evans discovered that the GStreamer 0.10 plugin to decode NES
    Sound Format files allowed the execution of arbitrary code.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.10.23-7.1+deb7u3.</p>

<p>We recommend that you upgrade your gst-plugins-bad0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-712.data"
# $Id: $
