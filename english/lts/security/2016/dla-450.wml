<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap-based buffer overflow has been discovered in gdk-pixbuf, a
library for image loading and saving facilities, fast scaling and
compositing of pixbufs, that allows remote attackers to cause a denial
of service or possibly execute arbitrary code via a crafted BMP file.</p>

<p>This update also fixes an incomplete patch for <a href="https://security-tracker.debian.org/tracker/CVE-2015-7674">CVE-2015-7674</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7552">CVE-2015-7552</a>

    <p>Heap-based buffer overflow in the gdk_pixbuf_flip function in
    gdk-pixbuf-scale.c in gdk-pixbuf allows remote attackers to cause a
    denial of service or possibly execute arbitrary code via a crafted
    BMP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7674">CVE-2015-7674</a>

    <p>Integer overflow in the pixops_scale_nearest function in
    pixops/pixops.c in gdk-pixbuf before 2.32.1 allows remote attackers
    to cause a denial of service (application crash) and possibly
    execute arbitrary code via a crafted GIF image file, which triggers
    a heap-based buffer overflow.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.26.1-1+deb7u4.</p>

<p>We recommend that you upgrade your gdk-pixbuf packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-450.data"
# $Id: $
