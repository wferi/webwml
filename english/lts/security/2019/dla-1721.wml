<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It has been discovered that OTRS (Open source Ticket Request System)
is susceptible to code injection vulnerability. An attacker who is
logged into OTRS as an agent or a customer user may upload a carefully
crafted resource in order to cause execution of JavaScript in the
context of OTRS. This is related to Content-type mishandling.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.3.18-1+deb8u8.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1721.data"
# $Id: $
