<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>George Noseevich discovered that firebird2.5, a relational database
system, did not properly check User-Defined Functions (UDF), thus
allowing remote authenticated users to execute arbitrary code on the
firebird server.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.5.2.26540.ds4-1~deb7u3.</p>

<p>We recommend that you upgrade your firebird2.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-879.data"
# $Id: $
