<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8361">CVE-2017-8361</a>

      <p>The flac_buffer_copy function in flac.c in libsndfile 1.0.28 allows
      remote attackers to cause a denial of service (buffer overflow and
      application crash) or possibly have unspecified other impact via a
      crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8362">CVE-2017-8362</a>

      <p>The flac_buffer_copy function in flac.c in libsndfile 1.0.28 allows
      remote attackers to cause a denial of service (invalid read and
      application crash) via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8363">CVE-2017-8363</a>

      <p>The flac_buffer_copy function in flac.c in libsndfile 1.0.28 allows
      remote attackers to cause a denial of service (heap-based buffer
      over-read and application crash) via a crafted audio file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8365">CVE-2017-8365</a>

      <p>The i2les_array function in pcm.c in libsndfile 1.0.28 allows
      remote attackers to cause a denial of service (buffer over-read
      and application crash) via a crafted audio file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.25-9.1+deb7u2.</p>

<p>We recommend that you upgrade your libsndfile packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-956.data"
# $Id: $
